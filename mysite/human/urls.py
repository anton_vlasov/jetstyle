from .views import HumanView, DetailView
from django.urls import path

app_name = 'human'

urlpatterns = [
    path('', HumanView.as_view(), name='humans'),
    path('<int:pk>', DetailView.as_view(), name='person'),
]
