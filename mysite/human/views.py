from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from .models import Human


class HumanView(generic.ListView):
    template_name = 'human/humans.html'
    context_object_name = 'humans_list'

    def get_queryset(self):
        return Human.objects.all()


class DetailView(generic.DetailView):
    template_name = 'human/person.html'
    model = Human

# Create your views here.
