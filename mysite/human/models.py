# from django.contrib.auth.models import 
from django.db import models


class Human(models.Model):
    name = models.CharField(max_length=32)
    age = models.IntegerField()
    # children = models.ManyToManyField('self', default=None)
    relatives = models.ManyToManyField('self', related_name='relatives', default=None)
    # children = models.ForeignKey('self', related_name='parent', on_delete=models.CASCADE, default=None, null=True)


    def __str__(self):
        return self.name
