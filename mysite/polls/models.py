from django.db import models
from django.utils import timezone
import datetime

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

    def __repr__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class OtherQueestion(models.Model):
    CHOICE1 = 'CH1'
    CHOICE2 = 'CH2'
    OTHER_Q_CHOICES = (
        (CHOICE1, 'CH1'),
        (CHOICE2, 'CH2'),
    )

    q_text = models.CharField(
        max_length=200,
        choices=OTHER_Q_CHOICES,
        default=CHOICE1,
    )
# Create your models here.
