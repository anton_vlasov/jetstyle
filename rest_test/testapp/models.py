from django.db import models
from django.conf import settings

from pygments.formatters.html import HtmlFormatter
from pygments import highlight


class Item(models.Model):
    name = models.CharField(blank=False, max_length=100)
    content = models.TextField(blank=True)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='items',
        on_delete=models.CASCADE
    )
    highlighted = models.TextField()

    class Meta:
        ordering = ('name',)

    

