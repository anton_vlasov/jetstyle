from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.test import APIRequestFactory
from rest_framework.request import Request

from .models import Item


factory = APIRequestFactory()
request = factory.get('/')

serializer_context = {
    'request': Request(request),
}


class UserSerializer(serializers.ModelSerializer):
    items = serializers.HyperlinkedRelatedField(
        view_name='item-detail',
        many=True,
        read_only=True,
    )

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'items')

    
class ItemSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    item = serializers.HyperlinkedRelatedField(
        view_name='item-detail',
        read_only=True,
    )

    class Meta:
        model = Item
        fields = ('name', 'id', 'content', 'owner', 'item')