from django.urls import path, include, re_path

from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register('users', views.UserViewSet)
# router.register('items', views.ItemViewSet)

# urlpatterns =[path('', include(router.urls)),]
urlpatterns = [
    path('items/', views.ItemList.as_view(), name='item-list'),
    path('items/<int:pk>', views.ItemDetail.as_view(), name='item-detail'),
]
urlpatterns += router.urls
