from django.urls import re_path, include

# from rest_framework import renderers
# from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view

from .views import UserViewSet, SnippetViewSet, api_root


scema_view = get_schema_view(title='Pastebin API')

router = DefaultRouter()
router.register('snippets', SnippetViewSet)
router.register('users', UserViewSet)

urlpatterns = [
    re_path(r'^', include(router.urls)),
    re_path(r'^schema/$', scema_view),
]

# snippet_list = SnippetViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })

# snippet_detail = SnippetViewSet.as_view({
#     'get': 'retrieve',
#     'put': 'update',
#     'patch': 'partial_update',
#     'delete': 'destroy'
# })

# snippet_highlight = SnippetViewSet.as_view(
#     {'get': 'highlight'},
#     renderer_classes=[renderers.StaticHTMLRenderer]
# )

# user_list = UserViewSet.as_view({
#     'get': 'list'
# })

# user_detail = SnippetViewSet.as_view({
#     'get': 'retrieve'
# })

# urlpatterns = [
#     re_path(r'^$', views.api_root),
#     re_path(
#         r'^snippets/$',
#         snippet_list,
#         name='snippet-list'
#     ),
#     re_path(
#         r'^snippets/(?P<pk>[0-9]+)/$',
#         snippet_detail,
#         name='snippet-detail'
#     ),
#     re_path(
#         r'^snippets/(?P<pk>[0-9]+)/highlight/$',
#         snippet_highlight,
#         name='snippet-highlight'
#     ),
#     re_path(
#         r'^users/$',
#         user_list,
#         name='user-list'
#     ),
#     re_path(
#         r'^users/(?P<pk>[0-9]+)/$',
#         user_detail,
#         name='user-detail'
#     ),
# ]

# urlpatterns = format_suffix_patterns(urlpatterns)
