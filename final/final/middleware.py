from django.shortcuts import redirect, reverse
from django.conf import settings

from re import match


class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        user = request.user
        path = request.path
        if (not user.is_authenticated
            and path not in settings.EXEMPT_URLS
            and not match(r'^/final/pbkdf2_sha1*', path)): # добавил условие, чтобы не ломалась хэш-ссылка
                return redirect(reverse('application:login'))
