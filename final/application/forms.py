from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm

from re import search

from .models import User


def password_validator(password):
    password_has_numbers = bool(search(r'\d', password))

    if not password_has_numbers:
        raise forms.ValidationError('Password must contain numbers!')


class LoginForm(forms.Form):
    username = forms.CharField(
        label='Username',
        required=True,
        widget=forms.TextInput,
    )
    password = forms.CharField(
        label='Enter password',
        required=True,
        widget=forms.PasswordInput(render_value=True),
        min_length=8
    )

    user = None

    error_messages = {}

    def is_user_active(self, username):
        user = User.objects.filter(username=username)
        if user:
            return user[0].is_active
        return False

    def clean(self):
        super().clean()
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']

        self.user = authenticate(
            username=username,
            password=password
        )

        if self.user is None:
            if not self.is_user_active(username):
            # он распоздает неактивного пользователя как NoneType, 
            # так что пришлось отдельный метод фигачить
                self.error_messages['msg'] = "User is inactive"
                raise forms.ValidationError(
                    self.error_messages['msg'],
                    code='msg'
                )
            # я не ставлю else, потому что prospector так говорит
            self.error_messages['msg'] = "Username and password doesn't match"
            raise forms.ValidationError(
                self.error_messages['msg'],
                code='msg'
            )

        return self.cleaned_data


class RegistrationForm(forms.Form):
    username = forms.CharField(
        label='Username',
        widget=forms.TextInput,
        required=True
    )
    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput,
        validators=[password_validator],
        required=True,
        min_length=8
    )
    password_confirmation = forms.CharField(
        label='Password confirmation',
        widget=forms.PasswordInput,
        required=True
    )
    email = forms.CharField(
        label='email',
        widget=forms.EmailInput,
        required=False
    )

    error_messages = {}

    def save(self):
        user = User.objects.create_user(
            username=self.cleaned_data.get('username'),
            password=self.cleaned_data.get('password_confirmation'),
            email=self.cleaned_data.get('email')
        )
        user.save()

    def set_and_raise_error(self, msg, username=None):
        if username is not None:
            self.error_messages['msg'] = msg % username
        else:
            self.error_messages['msg'] = msg

        raise forms.ValidationError(msg)

    def clean(self):
        super().clean()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirmation')

        if password != password_confirm:
            self.set_and_raise_error("Passwords didn't match")
        if User.objects.filter(username=username): # проверяю, существует ли юзер
            self.set_and_raise_error(
                "User with username %s already exists",
                username=username
            )

        return self.cleaned_data


class PasswordChangeForm(forms.Form):
    old_password = forms.CharField(
        label='old password',
        widget=forms.PasswordInput
    )
    new_password = forms.CharField(
        label='new password',
        validators=[password_validator],
        widget=forms.PasswordInput
    )
    new_password_confirmation = forms.CharField(
        label='confirm new password',
        widget=forms.PasswordInput
    )
    username = forms.CharField(
        widget=forms.HiddenInput
    )

    user = None # я к нему извне обращаюсь, поэтому он мне нужен:)
    error_messages = {}

    def set_error_msg(self, error_msg):
        self.error_messages['msg'] = error_msg
        raise forms.ValidationError(self.error_messages['msg'])

    def save(self):
        self.user.set_password(self.cleaned_data['new_password'])
        self.user.save()

    def clean(self):
        super().clean()
        old_password = self.cleaned_data['old_password']

        try:
            new_password = self.cleaned_data['new_password']
        except KeyError:
            self.set_error_msg('Password should consist of chars and numbers')

        new_password_confirmation = self.cleaned_data['new_password_confirmation']
        self.user = authenticate(
            username=self.cleaned_data['username'],
            password=old_password
        )

        is_user = self.user is not None
        password_not_old = (old_password != new_password)
        passwords_match = (new_password == new_password_confirmation)

        if not is_user:
            self.set_error_msg("Old password is wrong")
        if not password_not_old:
            self.set_error_msg("Password is same as the old")
        if not passwords_match:
            self.set_error_msg("Given passwords didn't match")

        return self.cleaned_data
