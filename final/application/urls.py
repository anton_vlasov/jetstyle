from django.urls import path, re_path

from . import views

app_name = 'application'
urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('register/', views.RegistrationView.as_view(), name='register'),
    path('profile/', views.AccountView.as_view(), name='profile'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('delete/', views.DeletionView.as_view(), name='delete'),
    path('profile/adminview/', views.AdminView.as_view(), name='adminview'),
    path('change/', views.PasswordChangeView.as_view(), name='change'),
    re_path(r'^pbkdf2_sha1*', views.hash_view)
]
