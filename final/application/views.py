from django.shortcuts import render, redirect, reverse
from django.contrib.auth import login, logout
from django.views import View
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.hashers import PBKDF2SHA1PasswordHasher

from .models import User
from .forms import LoginForm, RegistrationForm, PasswordChangeForm


HASHER = PBKDF2SHA1PasswordHasher()


def generate_hash(user):
    result = HASHER.encode(user.username, str(123))
    return result


def validate(request, path):
    """Сравнивает хэш в ссылке со свежепосчитанным для каждого юзера.
    Если нет совпадений - шлёт нафиг. Знаю, что долго, но я пока думал о том,
    что вообще мне нужно сделать и как реализовать. Я в том направлении думаю?
    Если что, чтоб авторизоваться, нужно после final/ вставить хэш, который указан на
    AccountView.
    """
    users = User.objects.all()
    for user in users:
        if not user.is_active: # неактивных юзеров за людей не считает, я не виноват
            continue
        if generate_hash(user) == path:
            login(request, user)
            return True
    return False


def hash_view(request):
    path = request.path
    if validate(request, path[7:]):
        return redirect(reverse('application:profile'))
    return redirect(reverse('application:login'))


class LoginView(View):
    form_class = LoginForm
    template_name = 'registration/login.html'

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, context={
            'form': form,
        })

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            login(request, form.user)
            return redirect('application:profile')

        return render(
            request,
            'registration/login.html',
            context={
                'form': self.form_class(initial={'username': request.POST['username']}),
                'error': form.error_messages.get('msg')
            }
        )


class RegistrationView(View):
    template_name = 'registration/register.html'
    form_class = RegistrationForm

    def get(self, request):
        return render(
            request,
            self.template_name,
            context={'form': self.form_class()}
        )

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('application:login'))
        response = render(
            request,
            self.template_name,
            context={
                'form': self.form_class(),
                'error': form.error_messages['msg']
            }
        )
        return response


class AccountView(View):
    template_name = 'application/profile.html'

    def get(self, request):
        response = render(
            request,
            self.template_name,
        )
        response.write(generate_hash(request.user))
        return response


class LogoutView(View):
    template_name = 'registration/logout.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        logout(request)
        return redirect(reverse('application:login'))


class DeletionView(View):
    template_name = 'registration/deletion.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        user = request.user
        user.delete()
        return redirect(reverse('application:login'))


class AdminView(UserPassesTestMixin, View):
    template_name = 'application/adminview.html'

    def get(self, request):
        return render(
            request,
            self.template_name,
            context={'users': User.objects.all()}
        )

    def test_func(self):
        return self.request.user.is_superuser


class PasswordChangeView(View):
    template_name = 'registration/change.html'
    form_class = PasswordChangeForm

    def get(self, request):
        return render(
            request,
            self.template_name,
            context={'form': self.form_class(
                initial={'username': request.user.username}
            )}
        )

    def post(self, request):
        form = self.form_class(request.POST)
        old_request = request #сохранил реквест, 
                              #потому что у меня теряется информация из него, если я этого не делаю
        if form.is_valid():
            form.save()
            login(request, form.user)
            return render(request, 'application/profile.html')
        return render(
                old_request,
                'registration/change.html',
                context={
                    'form': self.form_class(initial={'username': request.user.username}),
                    'error': form.error_messages['msg']
            }
        )
